<?php
namespace GYVSocket;


class ConnectorSocket {

	private $host;
	private $port;
	private $limitation;

	protected $scribe;

	protected $clients;
	protected $names;
	protected $online;
	protected $all;


	/**
	 * ConnectorSocket constructor.
	 * @param $host
	 * @param $port
	 * @param int $limitation
	 */
	public function __construct($host, $port, $limitation = 1024)
	{
		$this->scribe = new Scribe();


		$this->host = $host; //host
		$this->port = $port; //port
		$this->limitation = $limitation; //limitation in bit
		$this->null = NULL; //null var


		//Create TCP/IP sream socket
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

		//AF_INET - IPv4 Internet based protocols. TCP and UDP are common protocols of this protocol family.
		//reuseable port
		socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
		//timeout if header not sent
		socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec'=>1, 'usec'=>0));
		//bind socket to specified host
		socket_bind($socket, 0, $this->port);

		//listen to port
		socket_listen($socket);

		//create & add listning socket to the list
		$this->clients = array($socket);


		//start endless loop, so that our script doesn't stop
		while (true) {
			//manage multiple connections
			$changed = $this->clients;
			//returns the socket resources in $changed array
			socket_select($changed, $null, $null, 0, 10);

			//check for new socket
			if (in_array($socket, $changed)) {

				$socket_new = socket_accept($socket); //accept new socket

				$recived_data = socket_read($socket_new, $this->limitation); //read data sent by the socket
				$headers = $this->parce_recived_headers($recived_data);

				$this->onOpen($socket_new, $headers);



				//make room for new socket
				$found_socket = array_search($socket, $changed);
				unset($changed[$found_socket]);
			}

			//loop through all connected sockets
			foreach ($changed as $changed_socket) {

				//check for any incomming data
				while(socket_recv(
								$changed_socket,
								$buf,
								$this->limitation,
								0) >= 1)
				{

					DebugPrinter::printData([
							'$buf'=>$buf,
							'$this->clients'=>$this->clients,
							'$this->limitation'=>$this->limitation,
					], 'socket_recv');

					$received_text = $this->unmask($buf, $changed_socket); //unmask data

					$this->onMessage($changed_socket, $received_text);


					break 2; //exist this loop
				}


				$buf = @socket_read($changed_socket, $this->limitation, PHP_NORMAL_READ);
				if ($buf === false) { // check disconnected client

					$this->onClose($changed_socket);

				}
			}
		}
// close the listening socket
		$this->close_socket($socket);
		DebugPrinter::printData([

		], 'CLOSE BASE SOCKET');

	}

	protected function close_socket($socket){
		socket_close($socket);
	}

	protected function send_agreed($headers, $socket_new)
	{
		$secKey = (string) rand(100, 999);
		if (isset($headers['Sec-WebSocket-Key'])) {
			$secKey = $headers['Sec-WebSocket-Key'];
		}
		//hand shaking header
		$response_headers  = $this->scribe->get_response_headers($secKey, $this->host, $this->port);

		socket_write($socket_new,$response_headers,strlen($response_headers));

		DebugPrinter::printData([
				'$this->clients'=>$this->clients,

		], 'send_agreed');
	}

	protected function send_to_one($socket_self, $sending_message)
	{
		@socket_write($socket_self,$sending_message,strlen($sending_message));
		return true;
	}

	protected function send_to_all_without_self($socket_self, $sending_message)
	{
		foreach($this->clients as $changed_socket) {
			if ($changed_socket != $socket_self)
				$this->send_to_one($changed_socket, $sending_message);
		}
		return true;
	}


	protected function send_to_all($sending_message)
	{
		foreach($this->clients as $changed_socket) {
			$this->send_to_one($changed_socket, $sending_message);
		}
		return true;
	}

	function check_connection($id) {

		$return = in_array($id, array_column($this->all, 'id'));

		return $return;
	}


	protected function careful_close_socket($changed_socket) {
		$clients_keys = array_search($changed_socket, $this->clients);
		unset($this->clients[$clients_keys]);

		$this->close_socket($changed_socket);
	}

	protected function add_new_socket($socket_new)
	{
		$this->clients[] = $socket_new; //add socket to client array
	}




	//Unmask incoming framed message
	private function unmask($text, $changed_socket) {

		$return = $this->scribe->unmask($text);
		if (!$return) {
			$this->careful_close_socket($changed_socket);
			return false;
		}

		return $return;

	}




	private function parce_recived_headers($header)
	{
		$headers = array();
		$lines = preg_split("/\r\n/", $header);

		echo PHP_EOL.'$receved_header';
		print_r($header);
		echo PHP_EOL;

		foreach($lines as $line)
		{
			$line = chop($line);
			if(preg_match('/\A(GET|POST) \/\?(.*) HTTP/', $line, $matches))
			{
				print_r($matches);
				$headers[$matches[1]] = $matches[2];
			}
			if(preg_match('/\A(\S+): (.*)\z/', $line, $matches))
			{
				$headers[$matches[1]] = $matches[2];
			}

		}

		return $headers;
	}

	public function onOpen($conn, $headers) {}

	public function onMessage($from, $msg) {}

	public function onClose($conn) {}

	public function onError($conn, $error) {}
	
}

