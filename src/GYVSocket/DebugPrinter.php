<?php
/**
 * Created by PhpStorm.
 * User: gyv
 * Date: 20.05.2016
 * Time: 21:46
 */

namespace GYVSocket;


class DebugPrinter
{
    static function printData($params, $name='NAME', $var_damp = true )
    {
        echo PHP_EOL."<pre>".PHP_EOL;
        echo '------------------------------------------------------ ';
        echo PHP_EOL."-------------- {$name} ----------------- ";
        echo PHP_EOL.'------------------------------------------------------ ';
        foreach ($params as $key=>$param)
        {
            echo PHP_EOL.$key.PHP_EOL;
            ($var_damp)?var_dump($param):print_r($param);
            echo PHP_EOL;

        }
        echo PHP_EOL.'------------------------------------------------------ ';
        echo PHP_EOL."</pre>".PHP_EOL;

    }

}